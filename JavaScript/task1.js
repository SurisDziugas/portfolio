
const colors = require('../colors');

let hash = '';
const COLOR_LIST = [colors.red, colors.green, colors.yellow, colors.blue];

const getRandomColorList = (length) => { 
  const randomList = [];
  let index = 0;

  while(index < length){
    const random = ~~(Math.random() * COLOR_LIST.length);
    const randomColor = COLOR_LIST[random];
    if(randomColor !== randomList[index - 1]) {
      randomList.push(randomColor)
      index++;
    }
  }
  return randomList;
}

getRandomColorList(7).forEach(e => {
  hash += '#';  
  console.log(e(hash));
})


