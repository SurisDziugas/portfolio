function doStuff(text) {
  console.log(`Doing ${text}....`);
} 

function spy(func) {
  let runCount = 0;

  const spied = function () {
    runCount++;
    return func.apply(this, arguments)
  }; 

  spied.report = () => {
    return {
      totalCalls: runCount
    }
  }  
  return spied; 
}

const methodSpy = spy(doStuff);

methodSpy('something');
methodSpy('something else');
methodSpy('nothing');

console.log(methodSpy.report()); 
