// Given an array with nested arrays of numbers (ex.: [10, 6, [4, 8], 3, [6, 5, [9]]]) create
// a function that would sum all numbers from provided array.


const arr =  [10, 6, [4, 8], 3, [6, 5, [9]]];

const sumArray = (arr) => {
  let sum = 0;
  arr.forEach(element => {
    if(element.constructor === Array) {
      sum += sumArray(element);
    } else {
      sum += element;
    };
  });
  return sum;
}

console.log(sumArray(arr));