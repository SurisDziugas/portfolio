class Calculator {
  constructor(sum) {
    this.sum = sum;
  };

  add(number) {
    this.sum += number;
    return this;
  };

  subtract(number) {
    this.sum -= number;
    return this;
  };

  multiply(number) {
    this.sum *= number;
    return this;
  };

  divide(number) {
    this.sum /= number;
    return this;
  };

  valueOf() {
    return this.sum;
  };
}

const calc = new Calculator(0);
let amount = calc.add(5).multiply(2).add(20).divide(3);

console.log(+amount);
