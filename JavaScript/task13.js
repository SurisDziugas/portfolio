const say = (word) => {
  const sayMore = (additional) => {
   return `${word} ${additional}`;
  }
  return sayMore;
}

console.log( say("Hello,")("it’s me"));
