
// b)
const sum = (arr) => arr.reduce((a, b) => a + b);

// a)
const sum2 = (arr) => {
  let sum = 0;
  arr.forEach(e => {
    sum += e;
  });
  return sum;
}


console.log(sum2([5,5,5]))