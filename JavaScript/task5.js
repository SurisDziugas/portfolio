const houses = [
  {name: "Targaryen", motto: "Fire and Blood"},
  {name: "Stark", motto: "Winter is Coming"},
  {name: "Bolton", motto: "Our Blades Are Sharp"},
  {name: "Greyjoy", motto: "We Do Not Sow"},
  {name: "Tully", motto: "Family, Duty, Honor"},
  {name: "Arryn", motto: "As High as Honor"},
  {name: "Lannister", motto: "Hear Me Roar!"},
  {name: "Tyrell", motto: "Growing Strong"},
  {name: "Baratheon", motto: "Ours is the Fury"},
  {name: "Martell", motto: "Unbowed, Unbent, Unbroken"}
];

//es6
const es6GetMotto = (house) => houses.find(x => x.name === house).motto

//es5
const es5GetMotto = (house) => {
  let motto = '';
  houses.forEach( e => {
    if(e.name === house) {
      motto = e.motto;
    }
  })
  return motto ? motto : null;
}

console.log(es5GetMotto('Tyrell'));