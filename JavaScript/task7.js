const sentence = 'JavaScript yra, smagu.';

const wordSearch = (word, text) => text.split(/(\w+)/).indexOf(word) !== -1;

console.log(wordSearch('smagu', sentence));