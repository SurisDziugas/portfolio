const sevenAte9 = (word) => word.split('').filter(filter).join('')

const filter = (content, index, array) => {
  const currentElement = array[index] + '';
  const prevElement = array[index - 1] + '';
  const nextElement = array[index + 1] + '';
  return !(currentElement === '9' && prevElement === '7' && nextElement === '7');
}

console.log(sevenAte9('79712312'));