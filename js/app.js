import editMovie from './editMovie.js';
import getMovies from './getMovies.js';
import getMovie from './getMovie.js';
import newMovie from './newMovie.js';
import utility from './utility.js';

(function () {
  const url = new URL(window.location.href);
  const page = url.pathname;
  const movieID = url.searchParams.get('id');

  if(page === '/items.html') {
    getMovies();
  } else if(page === '/movie.html') {
    getMovie(movieID);
  } else if(page === '/edit.html') {
    movieID === 'new' ? newMovie() : editMovie(movieID);
  }

  utility();
})();


