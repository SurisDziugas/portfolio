import moviesRepos from './moviesRepo.js';

export default function editMovie (movieID) {

  const titleField = document.querySelector('#title');
  const authorField = document.querySelector('#author');
  const descriptionField = document.querySelector('#description');
  const saveButton = document.querySelector('.save');
  
  moviesRepos().get(movieID).then(response => response.json()).then(movie => {
    saveButton.value = movie.id;
    titleField.value = movie.title;
    authorField.value = movie.author;
    descriptionField.value = movie.description;
  });

  saveButton.addEventListener('click', () => {
    
    saveButton.disabled = true;

    const movie = {
      'id': movieID,
      'title': titleField.value,
      'author': authorField.value,
      'description': descriptionField.value
    }; 
    moviesRepos().update(movieID, movie).then(response => {
      if(response.status === 200) {
        window.location.href = './items.html';
      } else {
        alert('err');
      }
    }).catch(() => {
      saveButton.disabled = false;
    });

  });

}