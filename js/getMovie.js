import moviesRepos from './moviesRepo.js';

export default function getMovie (movieID) {
  moviesRepos().get(movieID).then(response =>  {
    if(response.status === 404) {
      window.location.href = './404.html';
      return;
    }
    return response.json();
  }).then(movie => {
    const pageTitle = document.querySelector('.page-title');
    const author = document.querySelector('.author');
    const description = document.querySelector('.description');

    pageTitle.innerHTML = movie.title;
    author.innerHTML = movie.author;
    description.innerHTML = movie.description;
  });
}