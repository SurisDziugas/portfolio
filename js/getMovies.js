import moviesRepos from './moviesRepo.js';


export default function getMovies () {
  moviesRepos().get().then(response => response.json()).then(movies => {
    const gridContainer = document.querySelector('.grid-items');
    if(movies.length > 0 ){
      movies.forEach(movie => gridContainer.appendChild(getRow(movie.id, movie.title, movie.author)));
      document.querySelectorAll('.movie-delete').forEach(button => button.addEventListener('click', deleteItem));
      sort();
      search();
      return;
    }
    gridContainer.innerHTML = '<p class="error">There are no movies ;(</p>';
  });

  function getRow (id, title, author) {
    const gridRow = document.createElement('div');
    gridRow.classList.add('grid-row');
    gridRow.innerHTML =  `<div class="flex-1 id"><p>${id}</p></div>
                          <h2 class="flex-5 title"><a class="movie-link" href="./movie.html?id=${id}"><p>${title}</p></a></h2>
                          <h3 class="flex-2 author"><p>${author}</p></h3>
                          <a  href="./edit.html?id=${id}" class="flex-1">Edit</a>
                          <button value="${id}" class="flex-1 movie-delete">Delete</button>`;
    return gridRow;
  }

  function deleteItem () {
    const button = event.target;
    button.disabled = true;
    moviesRepos().delete(button.value).then(response => response.status === 200 && button.closest('.grid-row').remove() );
  }

  function sort () {
    const sortHeads = document.querySelectorAll('.sort');
    const gridItems = document.querySelector('.grid-items');
    const sortState = getSortState();
    let isAscending = sortColumn(gridItems, sortState.column, sortState.isAscending);

    sortHeads.forEach(head => {
      head.addEventListener('click', e => {
        const sortAttr = e.target.id;

        isAscending = sortColumn(gridItems, sortAttr, isAscending);

        setSortState(sortAttr, isAscending);
      });
    });
  }

  function sortColumn (gridItems, sortAttr, isAscending) {
    const rows = [...document.querySelectorAll('.grid-row')];
    gridItems.innerHTML = '';
    rows.sort( (a, b) => {
      let element1 = a.querySelector(`.${sortAttr}`).textContent.toUpperCase();
      let element2 = b.querySelector(`.${sortAttr}`).textContent.toUpperCase();

      if(sortAttr === 'id') {
        element1 = Number(element1);
        element2 = Number(element2);
      }

      if (element1 < element2) {
        return isAscending ? -1 : 1;
      }
      if (element1 > element2) {
        return isAscending ? 1 : -1;
      }
      return 0;
    });

    rows.forEach(row => gridItems.appendChild(row) );

    return !isAscending;
  }

  function search () {
    document.querySelector('#search').addEventListener('input', field => {
      const searchWord = field.target.value.toLowerCase();
      const rows = document.querySelectorAll('.grid-row');

      if(searchWord == '') {
        rows.forEach(row => {
          row.classList.remove('hidden');
        });
        return;
      }

      rows.forEach(row => {
        const rowContents = [...row.querySelectorAll('p')].map(element => element.innerHTML.toLowerCase()).join(' ');
        if(rowContents.includes(searchWord)) {
          row.classList.remove('hidden');
        } else {
          row.classList.add('hidden');
        }
      });

    });
  }

  function setSortState(column, asc) {
    localStorage.setItem('column', column);
    localStorage.setItem('asc', asc);
  }

  function getSortState () {
    const column = localStorage.getItem('column') || 'id';
    const isAscending = localStorage.getItem('asc') || 'false';
    return {
      'column': column,
      'isAscending': isAscending == 'true' ? false : true
    };
  }

}

