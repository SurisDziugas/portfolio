export default function moviesRepos() {
  const url = 'http://localhost:3000/movies';

  function fetchMovies(id = '') {
    return fetch(`${url}/${id}`, {method: 'GET'});
  }

  function createMovie(data) {
    const bodyJson = JSON.stringify(data);
    return fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: bodyJson
    });
  }

  function deleteMovie(id) {
    return fetch(`${url}/${id}`, {method: 'DELETE'});
  }

  function updateMovie(id, data) {
    const bodyJson = JSON.stringify(data);
    return fetch(`${url}/${id}`, {
      method: 'PUT',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: bodyJson
    });
  }

  return {
    get: fetchMovies,
    create: createMovie,
    delete: deleteMovie,
    update: updateMovie
  };
}