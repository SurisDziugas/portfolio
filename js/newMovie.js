import moviesRepos from './moviesRepo.js';

export default function editMovie () {

  const titleField = document.querySelector('#title');
  const authorField = document.querySelector('#author');
  const descriptionField = document.querySelector('#description');
  const saveButton = document.querySelector('.save');

  saveButton.addEventListener('click', () => {
    if(titleField.value !== '' || authorField.value !== '' || descriptionField.value !== '') {
      const movie = {
        'title': titleField.value,
        'author': authorField.value,
        'description': descriptionField.value
      }; 
      moviesRepos().create(movie).then(response => {
        if(response.status === 201) {
          window.location.href = './items.html';
        } else {
          alert('err');
        }
      });
    }
  });



}