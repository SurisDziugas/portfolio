export default function utility() {
  function getSelectionText() {
    let selectedText = '';
    if (window.getSelection) {
      selectedText = window.getSelection().toString();
    }
    return selectedText;
  }

  function clearText() {
    if (window.getSelection) {
      if (window.getSelection().empty) {
        window.getSelection().empty();
      } else if (window.getSelection().removeAllRanges) {
        window.getSelection().removeAllRanges();
      }
    } else if (document.selection) {
      document.selection.empty();
    }
  }

  (function () {
    const paragraphs = document.querySelectorAll('p');
    const popup = document.querySelector('.share-popup');

    popup.querySelectorAll('a').forEach(link => {
      link.addEventListener('click', (e) => {
        popup.classList.add('hidden');
        alert(`${e.target.innerHTML} ${getSelectionText()}`);
        clearText();
      });
    });

    paragraphs.forEach(paragraph => {
      paragraph.addEventListener('mouseup', (e) => {
        const selectedText = getSelectionText();

        if (selectedText.length > 0) {
          const x = (e.clientX + 20) + 'px';
          const y = (e.clientY + 20) + 'px';
          popup.classList.remove('hidden');
          popup.style.top = y;
          popup.style.left = x;
        } else {
          popup.classList.add('hidden');
        }

      });
    });

    document.addEventListener('mousedown', (e) => {
      const selectedText = getSelectionText();
      if (!popup.classList.contains('hidden') && selectedText.length > 1 && !e.target.classList.contains('share-popup') ) {
        popup.classList.add('hidden');
        clearText();
      }
    });




  }());


}

